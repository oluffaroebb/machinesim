package fo.ofamth.machinesim;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Theme("mytheme")
public class MachineUI extends UI {
    private Machine machine = new Machine();
    private MyReceiver receiver;
    Upload upload;
    Table memory;
    Table registers;
    Label[] memLbl;
    Label[] regLbl;
    Button memoryReset;
    Button step;
    Button runBtn;
    Label runCount;
    int steps;
    boolean ready;
    TextField programCounter;
    TextField instrRegister;

    public MachineUI() {
        this.receiver = new MyReceiver(this, this.machine);
        this.upload = new Upload("Upload Program Here", this.receiver);
        this.memory = new Table("Memory");
        this.registers = new Table("Registers");
        this.memLbl = new Label[256];
        this.regLbl = new Label[16];
        this.memoryReset = new Button("Reset");
        this.step = new Button("Step");
        this.runBtn = new Button("Run");
        this.runCount = new Label();
        this.steps = 0;
        this.ready = true;
        this.programCounter = new TextField("Program Counter:");
        this.instrRegister = new TextField("Instruction Register:");
    }

    @Override
    public void init(VaadinRequest vaadinRequest) {
        this.machine.initMemory();
        this.upload.addSucceededListener(this.receiver);
        this.upload.setImmediate(true);
        this.instrRegister.setReadOnly(true);
        HorizontalLayout cntToolbar = new HorizontalLayout(new Component[]{this.programCounter, this.instrRegister, this.upload});
        cntToolbar.setSpacing(true);
        HorizontalLayout memoryToolbar = new HorizontalLayout(new Component[]{this.memoryReset, this.step, this.runBtn, this.runCount});
        memoryToolbar.setSpacing(true);
        VerticalLayout layout = new VerticalLayout();
        this.memoryReset.addClickListener((e) -> {
            this.machine.initMemory();
            this.machine.programCounter = 0;
            this.machine.instrRegister[0] = 0;
            this.machine.instrRegister[1] = 0;
            this.step.setEnabled(true);
            this.ready = true;
            this.runCount.setValue("");
            this.refreshMemory(true);
        });
        this.step.addClickListener((e) -> {
            this.executeStep();
        });
        this.runBtn.addClickListener((e) -> {
            this.executeRun();
        });
        this.programCounter.addValueChangeListener((e) -> {
            try {
                this.machine.programCounter = Integer.parseInt((String)this.programCounter.getValue(), 16);
                if (this.machine.programCounter >= 0 && this.machine.programCounter <= 255) {
                    this.step.setEnabled(true);
                    this.ready = true;
                    this.refreshMemoryPointer();
                } else {
                    Notification.show("Invalid program counter value " + this.machine.programCounter, Notification.Type.ERROR_MESSAGE);
                    this.machine.programCounter = 0;
                }
            } catch (NumberFormatException var3) {
                Notification.show("Invalid program counter value " + (String)this.programCounter.getValue(), Notification.Type.ERROR_MESSAGE);
            }

        });
        this.initMemory();
        this.initRegisters();
        layout.addComponents(new Component[]{cntToolbar, memoryToolbar, this.registers, this.memory});
        layout.setMargin(true);
        layout.setSpacing(true);
        this.refreshMemory(true);
        HorizontalLayout outerlayout = new HorizontalLayout(new Component[]{layout});
        this.setContent(outerlayout);
    }

    private void executeStep() {
        this.runCount.setValue("");

        try {
            if (this.ready && !this.machine.step()) {
                this.ready = false;
                this.step.setEnabled(false);
                ++this.steps;
                this.runCount.setValue("Steps: " + this.steps);
            } else {
                ++this.steps;
                this.runCount.setValue("Steps: " + this.steps);
            }
        } catch (Exception var2) {
            this.ready = false;
            this.step.setEnabled(false);
            var2.printStackTrace();
            Notification.show(var2.getMessage(), Notification.Type.ERROR_MESSAGE);
        }

        this.refreshMemory(false);
    }

    private void executeRun() {
        while(this.steps < 1000 && this.ready) {
            try {
                if (this.ready && !this.machine.step()) {
                    this.ready = false;
                    this.step.setEnabled(false);
                    ++this.steps;
                } else {
                    ++this.steps;
                }
            } catch (Exception var2) {
                this.ready = false;
                this.step.setEnabled(false);
                var2.printStackTrace();
                Notification.show(var2.getMessage(), Notification.Type.ERROR_MESSAGE);
            }
        }

        this.runCount.setValue("Steps: " + this.steps);
        this.refreshMemory(false);
    }

    private void initRegisters() {
        for(int i = 0; i < 16; ++i) {
            this.registers.addContainerProperty(String.format("%02X", i), Label.class, (Object)null);
        }

        Object[] row = new Object[16];

        for(int i = 0; i < 16; ++i) {
            this.regLbl[i] = new Label();
            this.regLbl[i].setValue(String.format("%02X", this.machine.register[i]));
            row[i] = this.regLbl[i];
        }

        this.registers.addItem(row, 1);
        this.registers.setPageLength(1);
    }

    private void initMemory() {
        int i;
        for(i = 0; i < 16; ++i) {
            this.memory.addContainerProperty("c" + i, Label.class, (Object)null);
        }

        for(i = 0; i < 256; ++i) {
            this.memLbl[i] = new Label();
            this.memLbl[i].setValue(i + ": 00");
        }

        for(i = 0; i < 16; ++i) {
            Object[] row = new Object[16];

            for(int j = 0; j < 16; ++j) {
                row[j] = this.memLbl[i + j * 16];
            }

            this.memory.addItem(row, i + 1);
        }

        this.memory.setPageLength(16);
        this.memory.setColumnHeaderMode(Table.COLUMN_HEADER_MODE_HIDDEN);
        Notification.show("Memory reset");
    }

    private void refreshMemoryPointer() {
        for(int i = 0; i < 256; ++i) {
            if (i == this.machine.programCounter) {
                this.memLbl[i].addStyleName("simunderline");
            } else {
                this.memLbl[i].removeStyleName("simunderline");
            }
        }

    }

    private void refreshMemory(boolean init) {
        int i;
        String s;
        for(i = 0; i < 256; ++i) {
            s = String.format("%02X", i) + ": " + String.format("%02X", this.machine.memory[i]);
            if (!this.memLbl[i].getValue().equals(s)) {
                this.memLbl[i].setValue(s);
                if (!init) {
                    this.memLbl[i].addStyleName("simbold");
                } else {
                    this.memLbl[i].removeStyleName("simbold");
                }
            } else {
                this.memLbl[i].removeStyleName("simbold");
            }

            if (i == this.machine.programCounter) {
                this.memLbl[i].addStyleName("simunderline");
            } else {
                this.memLbl[i].removeStyleName("simunderline");
            }
        }

        for(i = 0; i < 16; ++i) {
            s = String.format("%02X", this.machine.register[i]);
            if (!this.regLbl[i].getValue().equals(s)) {
                this.regLbl[i].setValue(s);
                this.regLbl[i].addStyleName("simbold");
            } else {
                this.regLbl[i].removeStyleName("simbold");
            }
        }

        this.instrRegister.setReadOnly(false);
        this.programCounter.setValue(String.format("%02X", this.machine.programCounter));
        this.instrRegister.setValue(String.format("%02X %02X", this.machine.instrRegister[0], this.machine.instrRegister[1]));
        this.instrRegister.setReadOnly(true);
    }

    class MyReceiver implements Upload.Receiver, Upload.SucceededListener {
        private Machine machine;
        private ByteArrayOutputStream s;
        private MachineUI myui;

        MyReceiver(MachineUI var1, Machine machine) {
            this.myui = var1;
            this.s = new ByteArrayOutputStream();
            this.machine = machine;
        }

        public OutputStream receiveUpload(String filename, String mimeType) {
            this.s = new ByteArrayOutputStream();
            return this.s;
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            try {
                int pCounter = this.machine.programCounter;
                this.machine.readProgram(this.s.toString(), pCounter);
                this.machine.programCounter = pCounter;
                this.myui.programCounter.setValue(String.format("%02X", pCounter));
            } catch (NumberFormatException var3) {
                var3.printStackTrace();
                Notification.show(var3.getMessage(), Notification.Type.ERROR_MESSAGE);
            } catch (IOException var4) {
                var4.printStackTrace();
                Notification.show(var4.getMessage(), Notification.Type.ERROR_MESSAGE);
            }
            myui.refreshMemory(false);
        }
    }
}
