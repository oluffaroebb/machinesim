package fo.ofamth.machinesim;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

public class Machine {
    int[] register = new int[16];
    int[] memory = new int[256];
    int programCounter = 0;
    int[] instrRegister = new int[2];
    final int lMask = 15;
    int opCode;
    int R;
    int T;
    int S;
    int X;
    int XY;
    String msg;

    public Machine() {
    }

    boolean fetch() {
        this.instrRegister[0] = this.memory[this.programCounter];
        this.instrRegister[1] = this.memory[this.programCounter + 1];
        this.programCounter += 2;
        return true;
    }

    int handleSign(int i) {
        if ((i & 128) > 0) {
            i |= -256;
        }

        return i;
    }

    boolean decode() throws Exception {
        this.opCode = this.instrRegister[0] >>> 4;
        switch(this.opCode) {
            case 1:
                this.R = this.instrRegister[0] & 15;
                this.XY = this.instrRegister[1];
                return true;
            case 2:
                this.R = this.instrRegister[0] & 15;
                this.XY = this.instrRegister[1];
                return true;
            case 3:
                this.R = this.instrRegister[0] & 15;
                this.XY = this.instrRegister[1];
                return true;
            case 4:
                this.R = this.instrRegister[1] >>> 4;
                this.S = this.instrRegister[1] & 15;
                return true;
            case 5:
                this.R = this.instrRegister[0] & 15;
                this.S = this.instrRegister[1] >>> 4;
                this.T = this.instrRegister[1] & 15;
                return true;
            case 6:
                this.R = this.instrRegister[0] & 15;
                this.S = this.instrRegister[1] >>> 4;
                this.T = this.instrRegister[1] & 15;
                return true;
            case 7:
                this.R = this.instrRegister[0] & 15;
                this.S = this.instrRegister[1] >>> 4;
                this.T = this.instrRegister[1] & 15;
                return true;
            case 8:
                this.R = this.instrRegister[0] & 15;
                this.S = this.instrRegister[1] >>> 4;
                this.T = this.instrRegister[1] & 15;
                return true;
            case 9:
                this.R = this.instrRegister[0] & 15;
                this.S = this.instrRegister[1] >>> 4;
                this.T = this.instrRegister[1] & 15;
                return true;
            case 10:
                this.R = this.instrRegister[0] & 15;
                this.X = this.instrRegister[1] & 15;
                return true;
            case 11:
                this.R = this.instrRegister[0] & 15;
                this.XY = this.instrRegister[1];
                return true;
            case 12:
                this.programCounter -= 2;
                return false;
            default:
                throw new Exception("Unknown op code");
        }
    }

    float convertToFloat(int x) {
        int sign = 1;
        float result = 0.0F;
        if ((x & 128) > 0) {
            sign = -1;
        }

        int i = x >>> 4 & 7;
        int exponent = i - 4;
        int mantissa = x & 15;
        result = (float)(mantissa >>> 3) * (float)Math.pow(2.0D, -1.0D);
        result += (float)(mantissa >>> 2 & 1) * (float)Math.pow(2.0D, -2.0D);
        result += (float)(mantissa >>> 1 & 1) * (float)Math.pow(2.0D, -3.0D);
        result += (float)(mantissa & 1) * (float)Math.pow(2.0D, -4.0D);
        result *= (float)Math.pow(2.0D, (double)exponent);
        result *= (float)sign;
        return result;
    }

    int convertFromFloat(float x) {
        int exponent = 0;
        int mantissa = 0;
        int sign = 0;
        int result = 0;
        if (x < 0.0F) {
            sign = 128;
            x = -x;
        }

        for(int i = 7; i >= 0; --i) {
            int n = (int)(x / 8.0F);
            if (n > 0) {
                mantissa += (int)Math.pow(2.0D, (double)i);
            }

            x -= (float)(n * 8);
            x *= 2.0F;
        }

        if (mantissa == 0) {
            exponent = 64;
        } else {
            if (mantissa < 8) {
                while(mantissa < 15) {
                    --exponent;
                    mantissa *= 2;
                }
            }

            if (mantissa > 15) {
                while(mantissa > 15) {
                    ++exponent;
                    mantissa /= 2;
                }
            }

            exponent += 4;
            exponent *= 16;
        }

        result = result | sign;
        result |= exponent;
        result |= mantissa;
        return result;
    }

    void execute() throws Exception {
        switch(this.opCode) {
            case 1:
                this.register[this.R] = this.memory[this.XY];
                break;
            case 2:
                this.register[this.R] = this.XY;
                break;
            case 3:
                this.memory[this.XY] = this.register[this.R];
                break;
            case 4:
                this.register[this.S] = this.register[this.R];
                break;
            case 5:
                this.register[this.R] = this.handleSign(this.register[this.S]) + this.handleSign(this.register[this.T]);
                this.register[this.R] &= 255;
                break;
            case 6:
                this.register[this.R] = this.convertFromFloat(this.convertToFloat(this.register[this.S]) + this.convertToFloat(this.register[this.T]));
                break;
            case 7:
                this.register[this.R] = this.register[this.S] | this.register[this.T];
                break;
            case 8:
                this.register[this.R] = this.register[this.S] & this.register[this.T];
                break;
            case 9:
                this.register[this.R] = this.register[this.S] ^ this.register[this.T];
                break;
            case 10:
                for(int cnt = this.X; cnt > 0; --cnt) {
                    boolean flag = (1 & this.register[this.R]) > 0;
                    this.register[this.R] >>>= 1;
                    if (flag) {
                        this.register[this.R] |= 128;
                    }
                }

                return;
            case 11:
                if (this.register[this.R] == this.register[0]) {
                    this.programCounter = this.XY;
                }
                break;
            default:
                throw new Exception("Unknown op code");
        }

    }

    public void setMemory(int offset, int[] m) {
        for(int i = 0; i < m.length; ++i) {
            this.memory[offset + i] = m[i];
        }

    }

    void go() throws Exception {
        while(this.fetch() && this.decode()) {
            this.execute();
        }

    }

    public void readProgram(String s, int memIndex) throws NumberFormatException, IOException {
        BufferedReader rdr = new BufferedReader(new StringReader(s));

        for(String line = rdr.readLine(); line != null; line = rdr.readLine()) {
            if (line.trim().length() == 2) {
                this.memory[memIndex] = Integer.parseInt(line, 16);
                ++memIndex;
            } else if (line.trim().length() == 4) {
                this.memory[memIndex] = Integer.parseInt(line.substring(0, 2), 16);
                ++memIndex;
                this.memory[memIndex] = Integer.parseInt(line.substring(2, 4), 16);
                ++memIndex;
            }
        }

    }

    boolean step() throws Exception {
        if (this.fetch() && this.decode()) {
            this.execute();
            return true;
        } else {
            return false;
        }
    }

    void initMemory() {
        int i;
        for(i = 0; i < this.memory.length; ++i) {
            this.memory[i] = 0;
        }

        for(i = 0; i < this.register.length; ++i) {
            this.register[i] = 0;
        }

        this.programCounter = 0;

        for(i = 0; i < this.instrRegister.length; ++i) {
            this.instrRegister[i] = 0;
        }

    }
}
